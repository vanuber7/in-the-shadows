﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAroundState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    [SerializeField] protected GuardController _controller;

    // Look around members
    [SerializeField] protected Transform _transform;
    protected Quaternion _oldRotation;
    protected Quaternion _targetRotation;
    [SerializeField] protected Vector3[] _lookDirections;
    protected int _numLookDirections;
    protected bool _isLookDirectionSet;
    [SerializeField] protected float _turnSpeed;
    [SerializeField] protected float _lookTargetTolerance;


    void Start()
    {
        _isLookDirectionSet = false;
        _numLookDirections = _lookDirections.Length;
    }


    public void EnterState()
    {
        if (!_isLookDirectionSet)
        {
            if (_numLookDirections > 0)
            {
                // Set the direction to turn and face in
                int nextDirection = Random.Range(0, _lookDirections.Length);
                _oldRotation = _transform.rotation;
                _targetRotation = _oldRotation * Quaternion.Euler(_lookDirections[nextDirection]);
                _isLookDirectionSet = true;
            }
        }
    }


    public void UpdateState()
    {
        if (_isLookDirectionSet)
        {
            _transform.rotation = Quaternion.Lerp(_transform.rotation, _targetRotation, Time.deltaTime * _turnSpeed);

            // Don't rotate all the way as it gets slower the nearer we get to the target
            float angle = Quaternion.Angle(_transform.rotation, _targetRotation);
            if (angle < _lookTargetTolerance)
            {
                _isLookDirectionSet = false;
                _behaviourTree.SetBool("Success", true);
            }
            else
            {
                _behaviourTree.SetBool("Running", true);
            }
        }
        else
        {
            _behaviourTree.SetBool("Failure", true);
        }
    }
}
