﻿using UnityEngine;
using Guard;
using UnityEngine.UI;

public class ThinkState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    [SerializeField] protected GuardController _controller;
    protected GuardSuspicionStatus _currentSuspicion;

    protected IGuardBehaviour _currentBehaviour;
    protected IGuardBehaviour _nextBehaviour;
    [SerializeField] protected PatrollingBehaviour _patrollingBehaviour;
    [SerializeField] protected SpeechBehaviour _speechBehaviour;
    [SerializeField] protected BoredBehaviour _boredBehaviour;

    protected bool _success;
    protected bool _failure;
    protected bool _running;

    [SerializeField, Range(0.0f, 1.0f)] protected float _lowVisualSuspicionValue;
    [SerializeField, Range(0.0f, 1.0f)] protected float _mediumVisualSuspicionValue;
    [SerializeField, Range(0.0f, 1.0f)] protected float _highVisualSuspicionValue;
    [SerializeField, Range(0.0f, 1.0f)] protected float _lowAudibleSuspicionValue;
    [SerializeField, Range(0.0f, 1.0f)] protected float _mediumAudibleSuspicionValue;
    [SerializeField, Range(0.0f, 1.0f)] protected float _highAudibleSuspicionValue;

    protected float _lastVisualSuspicionValue;
    protected float _lastAudibleSuspicionValue;
    protected float _lastAlertnessValue;

    // UI - Behaviour messages
    public Text _currentBehaviourText;
    public Text _nextBehaviourText;


    void Start()
    {
        _currentSuspicion = GuardSuspicionStatus.NO_SUSPICION;
        _currentBehaviour = null;
        _nextBehaviour = null;

        _success = false;
        _failure = false;
        _running = false;

        _lastVisualSuspicionValue = _controller._visualSuspiciousness;
        _lastAudibleSuspicionValue = _controller._audibleSuspiciousness;
        _lastAlertnessValue = _controller._alertness;

        UpdateBehaviourText();
    }


    public void EnterState()
    {
        CheckFlags();

        if(_running)
        {
            _running = false;
            if(HasSomethingChanged())
            {        
                MakeDecision();                
            }
            else
            {
                // If we reach here, nothing has changed so resume current behaviour
                _behaviourTree.SetTrigger(_currentBehaviour.GetBehaviourTrigger());
            }
        }
        else if (_success)
        {
            _success = false;
            if(!CheckForNextBehaviour())
            {
                MakeDecision();
            }
        }
        else if(_failure)
        {
            _failure = false;
            MakeDecision();
        }
        else
        {
            MakeDecision();
        }
    }


    public void UpdateState()
    {
    }


    protected void CheckFlags()
    {
        // Reset flag(s) for next state (if necessary)
        if (_behaviourTree.GetBool("Running"))
        {
            _running = true;
            _behaviourTree.SetBool("Running", false);
        }
        if (_behaviourTree.GetBool("Success"))
        {
            _success = true;
            _behaviourTree.SetBool("Success", false);
        }
        if (_behaviourTree.GetBool("Failure"))
        {
            _failure = true;
            _behaviourTree.SetBool("Failure", false);
        }
    }


    protected bool CheckForNextBehaviour()
    {
        // Does a specific behaviour follow on from this one?
        _nextBehaviour = _currentBehaviour.GetNextBehaviour();

        if (_nextBehaviour != null)
        {
            // Choose this next behaviour
            _currentBehaviour = _nextBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger(_nextBehaviour.GetBehaviourTrigger());
            return true;
        }

        // Behaviour has NOT been changed
        return false;
    }


    protected bool HasSomethingChanged()
    {
        if(_controller._visualSuspiciousness != _lastVisualSuspicionValue)
        {
            return true;
        }
        else if(_controller._audibleSuspiciousness != _lastAudibleSuspicionValue)
        {
            return true;
        }
        return false;
    }


    ///// MAKE DECISION /////
    protected void MakeDecision()
    {
        // Only enter 'bored' status when the Guard has no alertness AND suspicions
        if(_controller._alertness == 0.0f && 
            _controller._visualSuspiciousness < _lowVisualSuspicionValue &&
            _controller._audibleSuspiciousness < _lowAudibleSuspicionValue)
        {
            _currentBehaviour = _boredBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger("Bored");
        }
        // No suspicions -> Patrol
        else if (_controller._visualSuspiciousness < _lowVisualSuspicionValue && 
            _controller._audibleSuspiciousness < _lowAudibleSuspicionValue)
        {
            _lastVisualSuspicionValue = _controller._visualSuspiciousness;
            _lastAudibleSuspicionValue = _controller._audibleSuspiciousness;
            _currentBehaviour = _patrollingBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger("Patrol");
        }
        // Low visual suspicion
        else if(_controller._visualSuspiciousness >= _lowVisualSuspicionValue && 
                _controller._visualSuspiciousness < _mediumVisualSuspicionValue)
        {
            _lastVisualSuspicionValue = _controller._visualSuspiciousness;
            _currentSuspicion = GuardSuspicionStatus.VISUAL_LOW;
            _currentBehaviour = _speechBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger("Speech");
        }
        // Medium visual suspicion
        else if(_controller._visualSuspiciousness >= _mediumVisualSuspicionValue && 
                _controller._visualSuspiciousness < _highVisualSuspicionValue)
        {
            _lastVisualSuspicionValue = _controller._visualSuspiciousness;
            _currentSuspicion = GuardSuspicionStatus.VISUAL_MEDIUM;
            _currentBehaviour = _speechBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger("Speech");
        }
        // High visual suspicion
        else if (_controller._visualSuspiciousness >= _highVisualSuspicionValue)
        {
            _lastVisualSuspicionValue = _controller._visualSuspiciousness;
            _currentSuspicion = GuardSuspicionStatus.VISUAL_HIGH;
            _currentBehaviour = _speechBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger("Speech");
        }
        // Low audible suspicion
        else if (_controller._audibleSuspiciousness >= _lowAudibleSuspicionValue &&
                _controller._audibleSuspiciousness < _mediumAudibleSuspicionValue)
        {
            _lastAudibleSuspicionValue = _controller._audibleSuspiciousness;
            _currentSuspicion = GuardSuspicionStatus.AUDIBLE_LOW;
            _currentBehaviour = _speechBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger("Speech");
        }
        // Medium audible suspicion
        else if (_controller._audibleSuspiciousness >= _mediumAudibleSuspicionValue &&
                _controller._audibleSuspiciousness < _highAudibleSuspicionValue)
        {
            _lastAudibleSuspicionValue = _controller._audibleSuspiciousness;
            _currentSuspicion = GuardSuspicionStatus.AUDIBLE_MEDIUM;
            _currentBehaviour = _speechBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger("Speech");
        }
        // High audible suspicion
        else if (_controller._audibleSuspiciousness >= _highAudibleSuspicionValue)
        {
            _lastAudibleSuspicionValue = _controller._audibleSuspiciousness;
            _currentSuspicion = GuardSuspicionStatus.AUDIBLE_HIGH;
            _currentBehaviour = _speechBehaviour;
            UpdateBehaviourText();
            _behaviourTree.SetTrigger("Speech");
        }
    }


    public GuardSuspicionStatus GetCurrentSuspicion()
    {
        return _currentSuspicion;
    }


    protected void UpdateBehaviourText()
    {
        // Check for a valid behaviour before outputting
        if(_currentBehaviour != null)
        {
            _currentBehaviourText.text = "Current behaviour: " + _currentBehaviour.GetBehaviourTrigger();

            // Check for a following behaviour
            if (_currentBehaviour.GetNextBehaviour() != null)
            {                
                _nextBehaviourText.text = "Next behaviour: " + _currentBehaviour.GetNextBehaviour().GetBehaviourTrigger();
            }
            else
            {
                _nextBehaviourText.text = "Next behaviour: None";
            }
        }
    }
}