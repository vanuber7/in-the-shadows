﻿using System.Collections.Generic;
using UnityEngine;
using Guard;

public class DisbeliefSpeechState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    protected BehaviourTreeStatus _treeStatus;
    [SerializeField] protected GuardController _controller;
    
    [SerializeField] protected Transform _transform;

    [SerializeField] protected List<Quotes> _quotes;
    protected int _quoteListIndex;
    protected int _numQuotes;
    [HideInInspector] public float _quoteLength;


    public void EnterState()
    {
        // Did we see or hear the player?
        if(_controller._visualSuspiciousness > _controller._audibleSuspiciousness)
        {
            _quoteListIndex = 0;
        }
        else
        {
            _quoteListIndex = 1;
        }


        // Choose a random quote
        _numQuotes = _quotes[_quoteListIndex]._quotes.Count;
        int quoteNumber = Random.Range(0, _numQuotes);

        // Fetch the quote length and speak
        _quoteLength = AudioManager.GetInstance().GetClipLength(_quotes[_quoteListIndex]._quotes[quoteNumber].name);
        _treeStatus = AudioManager.GetInstance().PlaySound3D(_quotes[_quoteListIndex]._quotes[quoteNumber].name, _transform.position);

        if (_treeStatus == BehaviourTreeStatus.SUCCESS)
        {
            _behaviourTree.SetBool("Success", true);
        }
        else if (_treeStatus == BehaviourTreeStatus.FAILURE)
        {
            _behaviourTree.SetBool("Failure", true);
        }
    }


    public void UpdateState()
    {
    }
}
