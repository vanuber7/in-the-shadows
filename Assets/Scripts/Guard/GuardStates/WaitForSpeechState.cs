﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForSpeechState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    [SerializeField] protected GuardController _controller;
    [SerializeField] protected DisbeliefSpeechState _speakState;

    protected bool _isWaitTimeSet;
    protected float _waitTime;
     

    void Start()
    {
        _isWaitTimeSet = false;
        _waitTime = 0.0f;
    }


    public void EnterState()
    {
        if (!_isWaitTimeSet)
        {
            if (_speakState._quoteLength > 0.0f)
            {
                _waitTime = _speakState._quoteLength;
                _isWaitTimeSet = true;
            }
            else
            {
                _behaviourTree.SetBool("Failure", true);
            }
        }
    }


    public void UpdateState()
    {
        _waitTime -= Time.deltaTime;

        if (_waitTime < 0.0f)
        {
            _isWaitTimeSet = false;
            // We haven't seen or heard the player anymore so reset suspicions to prevent loop
            // Also hike up guard's alertness to increase sight/hearing range
            _controller.ResetSuspicions();
            _controller.ChangeAlertness(0.4f);
            _behaviourTree.SetBool("Success", true);
        }
        else
        {
            _behaviourTree.SetBool("Running", true);
        }
    }
}
