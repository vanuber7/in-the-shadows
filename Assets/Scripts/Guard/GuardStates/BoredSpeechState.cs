﻿using UnityEngine;
using Guard;

public class BoredSpeechState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    protected BehaviourTreeStatus _treeStatus;

    [SerializeField] protected Transform _transform;

    [SerializeField] protected Quotes _quotes;
    protected int _numQuotes;
    [HideInInspector] public float _quoteLength;


    public void EnterState()
    {
        // Choose a random quote
        _numQuotes = _quotes._quotes.Count;
        int quoteNumber = Random.Range(0, _numQuotes);

        // Fetch the quote length and speak
        _quoteLength = AudioManager.GetInstance().GetClipLength(_quotes._quotes[quoteNumber].name);
        _treeStatus = AudioManager.GetInstance().PlaySound3D(_quotes._quotes[quoteNumber].name, _transform.position);

        if (_treeStatus == BehaviourTreeStatus.SUCCESS)
        {
            _behaviourTree.SetBool("Success", true);
        }
        else if (_treeStatus == BehaviourTreeStatus.FAILURE)
        {
            _behaviourTree.SetBool("Failure", true);
        }
    }


    public void UpdateState()
    {
    }
}
