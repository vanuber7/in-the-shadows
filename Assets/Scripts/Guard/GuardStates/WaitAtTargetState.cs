﻿using UnityEngine;
using Guard;

public class WaitAtTargetState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    [SerializeField] protected GuardController _controller;

    [SerializeField, Range(0.1f, 5.0f)] protected float _waitTime;
    protected float _timeToWait;


    void Start()
    {
        _timeToWait = _waitTime;
    }


    public void EnterState()
    {
    }


    public void UpdateState()
    {
        _timeToWait -= Time.deltaTime;

        if (_timeToWait < 0.0f)
        {
            _timeToWait = _waitTime;
            // Reduce the Guard's alertness after each successfully navigated waypoint so that 'boredom'
            // is triggered when his alertness is zero.
            _controller.ChangeAlertness(-0.05f);
            _behaviourTree.SetBool("Success", true);
        }
        else
        {
            _behaviourTree.SetBool("Running", true);
        }
    }
}
