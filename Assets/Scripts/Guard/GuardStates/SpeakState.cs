﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Guard;

public class SpeakState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    [SerializeField] protected ThinkState _thinkState;
    protected GuardSuspicionStatus _suspicionStatus;
    protected BehaviourTreeStatus _treeStatus;

    [SerializeField] protected Transform _transform;

    [SerializeField] protected List<Quotes> _suspiciousnessQuotes;
    protected int _quoteListIndex;
    protected int _numQuotes;
    [HideInInspector] public float _quoteLength;

    protected bool _canSpeak;
    protected float _timeTillNextSpeech;


    protected void Start()
    {
        _canSpeak = true;
        _timeTillNextSpeech = 0.0f;
    }


    public void EnterState()
    {
        if (_canSpeak)
        {
            _suspicionStatus = _thinkState.GetCurrentSuspicion();

            switch (_suspicionStatus)
            {
                case GuardSuspicionStatus.VISUAL_LOW:
                    _quoteListIndex = 0;
                    break;

                case GuardSuspicionStatus.VISUAL_MEDIUM:
                    _quoteListIndex = 1;
                    break;

                case GuardSuspicionStatus.VISUAL_HIGH:
                    _quoteListIndex = 2;
                    break;

                case GuardSuspicionStatus.AUDIBLE_LOW:
                    _quoteListIndex = 3;
                    break;

                case GuardSuspicionStatus.AUDIBLE_MEDIUM:
                    _quoteListIndex = 4;
                    break;

                case GuardSuspicionStatus.AUDIBLE_HIGH:
                    _quoteListIndex = 5;
                    break;
            }

            // Choose a random quote
            _numQuotes = _suspiciousnessQuotes[_quoteListIndex]._quotes.Count;
            int quoteNumber = Random.Range(0, _numQuotes);

            // Fetch the quote length and speak
            _quoteLength = AudioManager.GetInstance().GetClipLength(_suspiciousnessQuotes[_quoteListIndex]._quotes[quoteNumber].name);
            _treeStatus = AudioManager.GetInstance().PlaySound3D(_suspiciousnessQuotes[_quoteListIndex]._quotes[quoteNumber].name, _transform.position);

            if (_treeStatus == BehaviourTreeStatus.SUCCESS)
            {
                // We have just spoken so prevent voice overlaps
                StartCoroutine(CountdownToNextSpeech());
                _behaviourTree.SetBool("Success", true);
            }
            else if (_treeStatus == BehaviourTreeStatus.FAILURE)
            {
                _behaviourTree.SetBool("Failure", true);
            }
        }
        else
        {
            _behaviourTree.SetBool("Failure", true);
        }
    }


    public void UpdateState()
    {
    }


    protected IEnumerator CountdownToNextSpeech()
    {
        // Helps to prevent speech spamming
        _canSpeak = false;
        _timeTillNextSpeech = _quoteLength;

        while(_timeTillNextSpeech > 0.0f)
        {
            _timeTillNextSpeech -= Time.deltaTime;
            yield return null;
        }

        _canSpeak = true;
        _timeTillNextSpeech = 0.0f;
    }
}