﻿using UnityEngine;
using UnityEngine.UI;
using Guard;

public class FindPlayerPositionState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    protected BehaviourTreeStatus _status;

    // Required to get the last known player position (Seen/heard)
    [SerializeField] protected GuardController _controller;
    [SerializeField] protected GuardNavigator _navigator;

    protected bool _isTargetSet;
    [SerializeField] protected Text _currentTargetText;


    void Start()
    {
        _isTargetSet = false;
        UpdateTargetText();
    }


    public void EnterState()
    {
        if (!_isTargetSet)
        {
            // Ensure the new destination is accepted
            _status = _navigator.SetNewDestination(_controller._lastKnownPlayerPosition);

            if (_status == BehaviourTreeStatus.SUCCESS)
            {
                _isTargetSet = true;
            }
            else if (_status == BehaviourTreeStatus.FAILURE)
            {
                _behaviourTree.SetBool("Failure", true);
            }
        }
    }


    public void UpdateState()
    {
        if (_isTargetSet)
        {
            _status = _navigator.FindPath();
            // Only proceed when the path is set AND calculated
            if (_status == BehaviourTreeStatus.SUCCESS)
            {
                _isTargetSet = false;

                // Stop the agent from moving automatically until walking state
                //_navigator.Stop();
                _behaviourTree.SetBool("Success", true);
            }
            else if (_status == BehaviourTreeStatus.RUNNING)
            {
                _behaviourTree.SetBool("Running", true);
            }
            else if (_status == BehaviourTreeStatus.FAILURE)
            {
                _isTargetSet = false;
                _behaviourTree.SetBool("Failure", true);
            }
        }
    }


    protected void UpdateTargetText()
    {
        string target = _controller._lastKnownPlayerPosition.ToString();
        _currentTargetText.text = "Current target: " + target;
    }
}
