﻿using UnityEngine;
using Guard;

public class WalkToTargetState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    protected BehaviourTreeStatus _status;
    [SerializeField] protected GuardNavigator _navigator;


    public void EnterState()
    {
        _navigator.SetMoveSpeed(0.4f);
        //_navigator.Resume();
    }


    public void UpdateState()
    {
        _status = _navigator.MoveToTarget();
        if (_status == BehaviourTreeStatus.SUCCESS)
        {
            _behaviourTree.SetBool("Success", true);
        }
        else if (_status == BehaviourTreeStatus.RUNNING)
        {
            _behaviourTree.SetBool("Running", true);
        }
        else
        {
            _behaviourTree.SetBool("Failure", true);
        }
    }
}
