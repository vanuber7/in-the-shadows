﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    [SerializeField] protected GuardNavigator _navigator;


    public void EnterState()
    {
        _navigator.Stop();
        _behaviourTree.SetBool("Success", true);
    }


    public void UpdateState()
    {
    }
}
