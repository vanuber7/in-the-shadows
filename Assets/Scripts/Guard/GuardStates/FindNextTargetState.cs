﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Guard;

public class FindNextTargetState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    protected BehaviourTreeStatus _status;

    [SerializeField] protected GuardNavigator _navigator;

    [SerializeField] protected Transform[] _patrolWaypoints;
    protected int _numTargets;
    protected int _currentTarget;
    protected bool _isTargetSet;
    [SerializeField] protected Text _currentTargetText;


    void Start()
    {
        _numTargets = _patrolWaypoints.Length;
        _currentTarget = 0;
        _isTargetSet = false;
        UpdateTargetText();
    }


    public void EnterState()
    {
        if (!_isTargetSet)
        {
            // Ensure the new destination is accepted
            _status = _navigator.SetNewDestination(_patrolWaypoints[_currentTarget].position);

            if(_status == BehaviourTreeStatus.SUCCESS)
            {
                UpdateTargetText();
                _isTargetSet = true;
            }
            else if (_status == BehaviourTreeStatus.FAILURE)
            {
                _behaviourTree.SetBool("Failure", true);
            }
        }
    }


    public void UpdateState()
    {
        if (_isTargetSet)
        {
            _status = _navigator.FindPath();
            // Only proceed when the path is set AND calculated
            if (_status == BehaviourTreeStatus.SUCCESS)
            {
                _isTargetSet = false;

                // Check next target index
                _currentTarget++;
                if (_currentTarget >= _numTargets)
                {
                    _currentTarget = 0;
                }

                // Stop the agent from moving automatically until walking state
                //_navigator.Stop();
                _behaviourTree.SetBool("Success", true);
            }
            else if (_status == BehaviourTreeStatus.RUNNING)
            {
                _behaviourTree.SetBool("Running", true);
            }
            else if (_status == BehaviourTreeStatus.FAILURE)
            {
                _isTargetSet = false;
                _behaviourTree.SetBool("Failure", true);
            }
        }
    }


    protected void UpdateTargetText()
    {
        string target = _patrolWaypoints[_currentTarget].position.ToString();
        _currentTargetText.text = "Current target: " + target;
    }
}
