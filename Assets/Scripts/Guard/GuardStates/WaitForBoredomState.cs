﻿using UnityEngine;

public class WaitForBoredomState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    [SerializeField] protected GuardController _controller;
    [SerializeField] protected BoredSpeechState _speakState;

    protected bool _isWaitTimeSet;
    protected float _waitTime;


    void Start()
    {
        _isWaitTimeSet = false;
        _waitTime = 0.0f;
    }


    public void EnterState()
    {
        if (!_isWaitTimeSet)
        {
            if (_speakState._quoteLength > 0.0f)
            {
                _waitTime = _speakState._quoteLength;
                _isWaitTimeSet = true;
            }
            else
            {
                _behaviourTree.SetBool("Failure", true);
            }
        }
    }


    public void UpdateState()
    {
        _waitTime -= Time.deltaTime;

        if (_waitTime < 0.0f)
        {
            _isWaitTimeSet = false;
            // After boredom is over, increase guard's alertness to an acceptable level
            // to prevent 'yawning' loop
            _controller.ChangeAlertness(0.2f);
            _behaviourTree.SetBool("Success", true);
        }
        else
        {
            _behaviourTree.SetBool("Running", true);
        }
    }
}
