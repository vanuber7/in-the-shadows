﻿using System.Collections.Generic;
using UnityEngine;
using Guard;

public class PatrolSequenceState : MonoBehaviour, IGuardState
{
    [SerializeField] protected Animator _behaviourTree;
    protected BehaviourTreeStatus _status;
    protected List<string> _patrolSequenceStates = new List<string>();

    protected int _numOfStates;
    protected int _currentState;

    // Tree flow control flags
    protected bool _success;
    protected bool _failure;
    protected bool _running;


    void Start()
    {
        _patrolSequenceStates.Add("FindNextTarget");
        _patrolSequenceStates.Add("WalkToTarget");
        _patrolSequenceStates.Add("WaitAtTarget");

        _numOfStates = _patrolSequenceStates.Count;
        _currentState = 0;
        _success = false;
        _failure = false;
        _running = false;
    }


    public void EnterState()
    {
        // Mark the success of the last task (Control passes back to 'Think')
        if (_behaviourTree.GetBool("Success"))
        {
            _success = true;
            _currentState++;

            // If we haven't completed the entire sequence yet...
            if (_currentState < _numOfStates)
            {
                // Return 'running' to the parent
                _behaviourTree.SetBool("Success", false);
                _behaviourTree.SetBool("Running", true);
            }
        }
        // Back from 'Think' - Move to next state
        else if (_success)
        {
            _success = false;

            if (_currentState >= _numOfStates)
            {
                _currentState = 0;
            }
            _behaviourTree.SetTrigger(_patrolSequenceStates[_currentState]);
        }
        // Mark the most recent task as running (Control passes back to 'Think')
        else if (_behaviourTree.GetBool("Running"))
        {
            _running = true;
        }
        // Back from 'Think' - Resume current state
        else if (_running)
        {
            _running = false;
            _behaviourTree.SetTrigger(_patrolSequenceStates[_currentState]);
        }
        // Just starting the sequence
        else
        {
            _behaviourTree.SetTrigger(_patrolSequenceStates[_currentState]);
        }
    }


    public void UpdateState()
    {
    }
}
