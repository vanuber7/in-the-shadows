﻿using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using Guard;

[RequireComponent(typeof(NavMeshAgent))]
public class GuardNavigator : MonoBehaviour
{
	[SerializeField] protected ThirdPersonCharacter _character;

    [SerializeField] protected NavMeshAgent _agent;
    [SerializeField] protected bool _targetExists;
    [SerializeField] protected float _distanceTolerance;


	///// START /////
	void Start()
	{
        // We only want the TPC to control our rotation
        _agent.updateRotation = false;
		_targetExists = false;
	}


    ///// STOP /////
    public void Stop()
	{
        Debug.Log("Stop");
        //Debug.Log(_agent.velocity);
        //_character.Move(Vector3.zero, false, false);
        //_agent.velocity = Vector3.zero;
        _agent.isStopped = true;
        //Debug.Log(_agent.velocity);
        //_agent.ResetPath();
        
        //_agent.enabled = false;
    }


	///// RESUME /////
	public void Resume()
	{
        Debug.Log("Resume");
        _agent.isStopped = false;
	}


	///// SET MOVE SPEED /////
	public void SetMoveSpeed(float newSpeed)
	{
		_agent.speed = newSpeed;
	}


    ///// SET NEW DESTINATION /////
    public BehaviourTreeStatus SetNewDestination(Vector3 newDestination)
    {
        if(!_targetExists)
        {
            if(_agent.SetDestination(newDestination))
            {
                Debug.Log("Next target: " + newDestination);
                _targetExists = true;
                return BehaviourTreeStatus.SUCCESS;
            }
        }
        // Failed to request a target for the agent
        return BehaviourTreeStatus.FAILURE;
    }


	///// FIND PATH /////
	public BehaviourTreeStatus FindPath()
	{
        if (_targetExists)
        {
            // The path can take a few frames to calculate
            if(_agent.pathPending)
            {
                return BehaviourTreeStatus.RUNNING;
            }

            // Do we have a valid path?
            switch (_agent.pathStatus)
            {
                case NavMeshPathStatus.PathComplete:
                    _targetExists = false;
                    Debug.Log("Path calculated");
                    return BehaviourTreeStatus.SUCCESS;

                case NavMeshPathStatus.PathPartial:
                case NavMeshPathStatus.PathInvalid:
                    Debug.Log("Path invalid");
                    return BehaviourTreeStatus.FAILURE;
            }
        }
        return BehaviourTreeStatus.FAILURE;
    }


	///// MOVE TO TARGET /////
	public BehaviourTreeStatus MoveToTarget()
	{
        //** Grabbed from Unity's 'AICharacterControl'
        if (_agent.remainingDistance > _agent.stoppingDistance)
        {
            _character.Move(_agent.desiredVelocity, false, false);
        }
        else
        {
            _character.Move(Vector3.zero, false, false);
        }

        //Debug.Log("Remaining distance = " + _agent.remainingDistance);

        if(_agent.remainingDistance != Mathf.Infinity &&
            _agent.remainingDistance < _agent.stoppingDistance)
        {
            //Debug.Log("Agent's magnitude = " + _agent.velocity.sqrMagnitude);
            if (!_agent.hasPath || _agent.velocity.sqrMagnitude == 0.0f)
            {
                //Stop();
                Debug.Log("Target reached");
                return BehaviourTreeStatus.SUCCESS;
            }
        }
        //else if(_agent.remainingDistance > _agent.stoppingDistance)
        //{
        //    if(_agent.velocity.sqrMagnitude == 0.0f)
        //    {
        //        // We've stopped moving and are not close to target - Quit out
        //        Debug.Log("Failed to reach target");
        //        return BehaviourTreeStatus.FAILURE;
        //    }
        //}
        return BehaviourTreeStatus.RUNNING;
	}
}
