﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class GuardController : MonoBehaviour, IInteractable, IVisible
{
    // AI members
    [SerializeField] protected Animator _behaviourTree;
    protected Dictionary<string, IGuardBehaviour> _guardBehaviourDict = new Dictionary<string, IGuardBehaviour>();
    [SerializeField] protected ThinkingBehaviour _thinkingBehaviour;
    [SerializeField] protected PatrollingBehaviour _patrollingBehaviour;
    [SerializeField] protected BoredBehaviour _boredBehaviour;
    [SerializeField] protected SpeechBehaviour _speechBehaviour;
    [SerializeField] protected LocateAndSearchBehaviour _locateAndSearchBehaviour;
    [SerializeField] protected DisbeliefBehaviour _disbeliefBehaviour;

    // Sense members
    [SerializeField] protected GuardVisionSense _visionSense;
    [SerializeField] protected GuardHearingSense _hearingSense;
	[SerializeField, Range(0.0f, 1.0f)] public float _visualSuspiciousness;
    protected int _visualSuspicionLevel;
    [SerializeField, Range(0.0f, 1.0f)] public float _audibleSuspiciousness;
    protected int _audibleSuspicionLevel;
    [SerializeField, Range(0.0f, 1.0f)] public float _alertness;

    // Interactable members
    protected List<IInteractable> _interactablesInProximity = new List<IInteractable>();
	[SerializeField, Range(0.1f, 1.0f)] protected float _checkInterval;
	[SerializeField] protected LayerMask _guardIgnoreMask;
	[SerializeField, Range(0.5f, 2.0f)] protected float _touchRange;

    // Spotting points
    [HideInInspector] public Vector3 _lastKnownPlayerPosition;

    // UI suspicion meters
    [SerializeField] protected Image _eyeUI;
    [SerializeField] protected Sprite[] _eyeSprites;
    [SerializeField] protected Image _leftEarUI;
    [SerializeField] protected Image _rightEarUI;
    [SerializeField] protected Sprite[] _earSprites;
    [SerializeField] protected Image _alertnessUI;
    [SerializeField] protected Color[] _alertnessColours;


    ///// START /////
    void Start()
	{
        // Subscribe to all behaviour tree events (Brain)
        foreach (GuardBehaviour behaviour in _behaviourTree.GetBehaviours<GuardBehaviour>())
        {
            behaviour.onStateEnterEvent += OnStateEnterEvent;
            behaviour.onStateUpdateEvent += OnStateUpdateEvent;
            behaviour.onStateExitEvent += OnStateExitEvent;
        }

        // Add guard behaviour states
        _guardBehaviourDict.Add("Think", _thinkingBehaviour);
        _guardBehaviourDict.Add("Patrol", _patrollingBehaviour);
        _guardBehaviourDict.Add("Speech", _speechBehaviour);
        _guardBehaviourDict.Add("LocateAndSearch", _locateAndSearchBehaviour);
        _guardBehaviourDict.Add("Disbelief", _disbeliefBehaviour);
        _guardBehaviourDict.Add("Bored", _boredBehaviour);

        // Subscribe to 'sense' events
		_visionSense.onSawObject += OnSawObject;
		_hearingSense.onHeardSound += OnHeardSound;

        _audibleSuspicionLevel = 0;
        _visualSuspicionLevel = 0;
        _alertnessUI.color = _alertnessColours[0];
        UpdateVisualUI();
        UpdateAudibleUI();
        UpdateAlertnessUI();

        _lastKnownPlayerPosition = Vector3.zero;

        // Ignore all guard layer (invert bit mask)
        _guardIgnoreMask = ~_guardIgnoreMask;
		StartCoroutine(CheckEnvironment());
	}


    public void OnStateEnterEvent(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach(KeyValuePair<string, IGuardBehaviour> pair in _guardBehaviourDict)
        {
            if(stateInfo.IsTag(pair.Key))
            {
                pair.Value.EnterBehaviour(stateInfo);
                break;
            }
        }
    }


    public void OnStateUpdateEvent(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach (KeyValuePair<string, IGuardBehaviour> pair in _guardBehaviourDict)
        {
            if (stateInfo.IsTag(pair.Key))
            {
                pair.Value.UpdateBehaviour(stateInfo);
                break;
            }
        }
    }


    public void OnStateExitEvent(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // No behaviours currently use this
    }


    ///// CHECK ENVIRONMENT (Co-routine) /////
    protected IEnumerator CheckEnvironment()
	{
		while (true)
		{
			CheckForInteractables();
			yield return new WaitForSeconds(_checkInterval);
		}
	}


	///// INTERACT (IInteractable) /////
	public void Interact()
	{
		Debug.Log("Interacted with: " + gameObject.name);
	}


	///// SHOW  (IVisible) /////
	public void Show()
	{
		Debug.Log("I am " + gameObject.name);
	}


	///// ON SAW OBJECT (Event - GuardVisionSense) /////
	void OnSawObject(GameObject objectSeen)
	{
		Debug.Log(gameObject.name + " saw " + objectSeen);
		
        if(objectSeen.CompareTag("Player"))
        {
            ChangeVisualSuspicion(0.1f);
            _lastKnownPlayerPosition = objectSeen.transform.position;
        }
	}


    ///// CHANGE VISUAL SUSPICION /////
    public void ChangeVisualSuspicion(float change)
    {
        _visualSuspiciousness = Mathf.Clamp(_visualSuspiciousness + change, 0.0f, 1.0f);
        UpdateVisualUI();

        if(Mathf.Approximately(_visualSuspiciousness, 1.0f))
        {
            GameManager.GetInstance().TriggerGameEnded(false);
        }

        Debug.Log("Visual suspiciousness = " + _visualSuspiciousness);
    }


    ///// ON HEARD SOUND (Event - GuardHearingSense) /////
    void OnHeardSound(AudioSource soundHeard)
	{
		Debug.Log(gameObject.name + " heard " + soundHeard.name);

        if(soundHeard.CompareTag("Player"))
        {
            ChangeAudibleSuspicion(0.2f);
            _lastKnownPlayerPosition = soundHeard.transform.position;
        }
	}


    ///// CHANGE AUDIBLE SUSPICION /////
    public void ChangeAudibleSuspicion(float change)
    {
        _audibleSuspiciousness = Mathf.Clamp(_audibleSuspiciousness + change, 0.0f, 1.0f);
        UpdateAudibleUI();

        if (Mathf.Approximately(_audibleSuspiciousness, 1.0f))
        {
            GameManager.GetInstance().TriggerGameEnded(false);
        }

        Debug.Log("Audible suspiciousness = " + _audibleSuspiciousness);
    }


    public void ResetSuspicions()
    {
        _visualSuspiciousness = 0.0f;
        _audibleSuspiciousness = 0.0f;
        UpdateVisualUI();
        UpdateAudibleUI();
    }


    public void ChangeAlertness(float change)
    {
        _alertness = Mathf.Clamp(_alertness + change, 0.0f, 1.0f);
        _visionSense.ChangeVisionRange(_alertness);
        _hearingSense.ChangeHearingRadius(_alertness);
        UpdateAlertnessUI();

        Debug.Log("Alertness = " + _alertness);
    }


    protected void UpdateVisualUI()
    {
        if (_visualSuspiciousness < 0.1f)
        {
            // Visual suspicion has been reset to zero
            if (_visualSuspicionLevel > 1)
            {
                StartCoroutine(EyeUICooldown());
            }
            else
            {
                _visualSuspicionLevel = 0;
                _eyeUI.sprite = _eyeSprites[_visualSuspicionLevel];
            }
        }
        else if (_visualSuspiciousness >= 0.1f && _visualSuspiciousness < 0.4f)
        {
            // Low visual suspicion
            _visualSuspicionLevel = 1;
            _eyeUI.sprite = _eyeSprites[_visualSuspicionLevel];
        }
        else if (_visualSuspiciousness >= 0.4f && _visualSuspiciousness < 0.8f)
        {
            // Medium visual suspicion
            _visualSuspicionLevel = 2;
            _eyeUI.sprite = _eyeSprites[_visualSuspicionLevel];
        }
        else if (_visualSuspiciousness >= 0.8f)
        {
            // High visual suspicion
            _visualSuspicionLevel = 3;
            _eyeUI.sprite = _eyeSprites[_visualSuspicionLevel];
        }
    }


    protected IEnumerator EyeUICooldown()
    {
        float cooldownTimer = 1.0f;

        // Reduce guard's eye UI down to zero gradually
        while (_visualSuspicionLevel > 0)
        {
            cooldownTimer -= Time.deltaTime;

            if (cooldownTimer < 0.0f)
            {
                cooldownTimer = 1.0f;
                _visualSuspicionLevel--;
                _eyeUI.sprite = _eyeSprites[_visualSuspicionLevel];
            }
            yield return null;
        }
        yield return null;
    }


    protected void UpdateAudibleUI()
    {
        if (_audibleSuspiciousness < 0.1f)
        {
            // Audible suspicion has been reset to zero
            if (_audibleSuspicionLevel > 1)
            {
                StartCoroutine(EarUICooldown());
            }
            else
            {
                _audibleSuspicionLevel = 0;
                _leftEarUI.sprite = _earSprites[_audibleSuspicionLevel];
                _rightEarUI.sprite = _earSprites[_audibleSuspicionLevel];
            }
        }
        else if (_audibleSuspiciousness >= 0.1f && _audibleSuspiciousness < 0.4f)
        {
            // Low audible suspicion
            _audibleSuspicionLevel = 1;
            _leftEarUI.sprite = _earSprites[_audibleSuspicionLevel];
            _rightEarUI.sprite = _earSprites[_audibleSuspicionLevel];
        }
        else if (_audibleSuspiciousness >= 0.4f && _audibleSuspiciousness < 0.8f)
        {
            // Medium audible suspicion
            _audibleSuspicionLevel = 2;
            _leftEarUI.sprite = _earSprites[_audibleSuspicionLevel];
            _rightEarUI.sprite = _earSprites[_audibleSuspicionLevel];
        }
        else if (_audibleSuspiciousness >= 0.8f)
        {
            // High audible suspicion
            _audibleSuspicionLevel = 3;
            _leftEarUI.sprite = _earSprites[_audibleSuspicionLevel];
            _rightEarUI.sprite = _earSprites[_audibleSuspicionLevel];
        }
    }


    protected IEnumerator EarUICooldown()
    {
        float cooldownTimer = 1.0f;

        // Reduce guard's ear UI down to zero gradually
        while(_audibleSuspicionLevel > 0)
        {
            cooldownTimer -= Time.deltaTime;

            if(cooldownTimer < 0.0f)
            {
                cooldownTimer = 1.0f;
                _audibleSuspicionLevel--;
                _leftEarUI.sprite = _earSprites[_audibleSuspicionLevel];
                _rightEarUI.sprite = _earSprites[_audibleSuspicionLevel];
            }
            yield return null;
        }
        yield return null;
    }


    protected void UpdateAlertnessUI()
    {
        // Change alertness UI colour in stages
        if(Mathf.Approximately(_alertness, 0.0f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[0]));
        }
        else if (Mathf.Approximately(_alertness, 0.1f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[1]));
        }
        else if (Mathf.Approximately(_alertness, 0.2f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[2]));
        }
        else if (Mathf.Approximately(_alertness, 0.3f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[3]));
        }
        else if (Mathf.Approximately(_alertness, 0.4f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[4]));
        }
        else if (Mathf.Approximately(_alertness, 0.5f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[5]));
        }
        else if (Mathf.Approximately(_alertness, 0.6f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[6]));
        }
        else if (Mathf.Approximately(_alertness, 0.7f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[7]));
        }
        else if (Mathf.Approximately(_alertness, 0.8f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[8]));
        }
        else if (Mathf.Approximately(_alertness, 0.9f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[9]));
        }
        else if (Mathf.Approximately(_alertness, 1.0f))
        {
            StartCoroutine(LerpToColour(_alertnessColours[10]));
        }
    }


    protected IEnumerator LerpToColour(Color targetColour)
    {
        bool finished = false;

        while (!finished)
        {
            _alertnessUI.color = Color.Lerp(_alertnessUI.color, targetColour, 1.0f * Time.deltaTime);

            if(_alertnessUI.color == targetColour)
            {
                finished = true;
            }
            yield return null;
        }
        yield return null;
    }


    ///// CHECK FOR INTERACTABLES /////
    protected void CheckForInteractables()
	{
		// Conduct a small radius check for objects
		Collider[] touchableObjects = Physics.OverlapSphere(transform.position, _touchRange, _guardIgnoreMask);

		if (touchableObjects.Length > 0)
		{
			foreach (Collider col in touchableObjects)
			{
				// Is this object an interactable?
				IInteractable interactable = col.gameObject.GetComponent<IInteractable>();

				if (interactable != null)
				{
					_interactablesInProximity.Add(interactable);
				}
			}

			foreach (IInteractable interactable in _interactablesInProximity)
			{
				// DO STUFF WITH INTERACTABLES
			}

			// Empty list for next check
			_interactablesInProximity.Clear();
		}
	}


    ///// ON DESTROY /////
    protected void OnDestroy()
	{
        // Unsubscribe from all behaviours
        foreach (GuardBehaviour behaviour in _behaviourTree.GetBehaviours<GuardBehaviour>())
        {
            behaviour.onStateEnterEvent -= OnStateEnterEvent;
            behaviour.onStateUpdateEvent -= OnStateUpdateEvent;
            behaviour.onStateExitEvent -= OnStateExitEvent;
        }

        _visionSense.onSawObject -= OnSawObject;
		_hearingSense.onHeardSound -= OnHeardSound;
	}
}