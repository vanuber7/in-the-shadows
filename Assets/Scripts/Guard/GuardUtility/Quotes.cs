﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Quotes", menuName = "GuardQuotes/Quotes", order = 1)]
public class Quotes : ScriptableObject
{
    // Create this object from the 'Assets' menu
    public List<AudioSource> _quotes;
}
