﻿using System.Collections.Generic;
using UnityEngine;


public class ThinkingBehaviour : MonoBehaviour, IGuardBehaviour
{
    protected ThinkState _guardThinkState;


    ///// START /////
    void Start()
    {
        _guardThinkState = GetComponent<ThinkState>();
    }

    public void EnterBehaviour(AnimatorStateInfo stateInfo)
    {
        _guardThinkState.EnterState();
    }


    public void UpdateBehaviour(AnimatorStateInfo stateInfo)
    {
    }


    public IGuardBehaviour GetNextBehaviour()
    {
        return null;
    }


    public string GetBehaviourTrigger()
    {
        return "Think";
    }
}
