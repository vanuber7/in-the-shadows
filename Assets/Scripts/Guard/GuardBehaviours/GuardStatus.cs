﻿namespace Guard
{
    public enum BehaviourTreeStatus
    {
        SUCCESS,
        FAILURE,
        RUNNING
    }


    public enum GuardSuspicionStatus
    {
        VISUAL_LOW,
        VISUAL_MEDIUM,
        VISUAL_HIGH,
        AUDIBLE_LOW,
        AUDIBLE_MEDIUM,
        AUDIBLE_HIGH,
        NO_SUSPICION
    }
}