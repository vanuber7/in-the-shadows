﻿using UnityEngine;

public class GuardBehaviour : StateMachineBehaviour
{
    public event OnStateEnterEvent onStateEnterEvent;
    public delegate void OnStateEnterEvent(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);

    public event OnStateUpdateEvent onStateUpdateEvent;
    public delegate void OnStateUpdateEvent(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);

    public event OnStateExitEvent onStateExitEvent;
    public delegate void OnStateExitEvent(Animator animator, AnimatorStateInfo stateInfo, int layerIndex);


    ///// ON STATE ENTER /////
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
        if(onStateEnterEvent != null)
        {
            onStateEnterEvent(animator, stateInfo, layerIndex);
        }
	}


    ///// ON STATE UPDATE /////
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (onStateUpdateEvent != null)
        {
            onStateUpdateEvent(animator, stateInfo, layerIndex);
        }
    }


    ///// ON STATE EXIT /////
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (onStateExitEvent != null)
        {
            onStateExitEvent(animator, stateInfo, layerIndex);
        }
    }
}
