﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisbeliefBehaviour : MonoBehaviour, IGuardBehaviour
{
    protected Dictionary<string, IGuardState> _disbeliefStatesDict = new Dictionary<string, IGuardState>();
    [SerializeField] protected DisbeliefSequenceState _disbeliefSequenceState;
    [SerializeField] protected DisbeliefSpeechState _disbeliefSpeechState;
    [SerializeField] protected WaitForSpeechState _waitForSpeechState;


    void Start()
    {
        _disbeliefStatesDict.Add("Disbelief", _disbeliefSequenceState);
        _disbeliefStatesDict.Add("DisbeliefSpeech", _disbeliefSpeechState);
        _disbeliefStatesDict.Add("WaitForSpeech", _waitForSpeechState);
    }


    public void EnterBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _disbeliefStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.EnterState();
                break;
            }
        }
    }


    public void UpdateBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _disbeliefStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.UpdateState();
                break;
            }
        }
    }


    public IGuardBehaviour GetNextBehaviour()
    {
        return null;
    }


    public string GetBehaviourTrigger()
    {
        return "Disbelief";
    }
}
