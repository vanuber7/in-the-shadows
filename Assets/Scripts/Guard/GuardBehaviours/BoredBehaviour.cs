﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoredBehaviour : MonoBehaviour, IGuardBehaviour
{
    protected Dictionary<string, IGuardState> _boredStatesDict = new Dictionary<string, IGuardState>();
    [SerializeField] protected BoredSequenceState _boredSequenceState;
    [SerializeField] protected BoredSpeechState _boredSpeechState;
    [SerializeField] protected WaitForBoredomState _waitForBoredomState;


    void Start()
    {
        _boredStatesDict.Add("Bored", _boredSequenceState);
        _boredStatesDict.Add("BoredSpeech", _boredSpeechState);
        _boredStatesDict.Add("WaitForBoredom", _waitForBoredomState);
    }


    public void EnterBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _boredStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.EnterState();
                break;
            }
        }
    }


    public void UpdateBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _boredStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.UpdateState();
                break;
            }
        }
    }


    public IGuardBehaviour GetNextBehaviour()
    {
        return null;
    }


    public string GetBehaviourTrigger()
    {
        return "Bored";
    }
}
