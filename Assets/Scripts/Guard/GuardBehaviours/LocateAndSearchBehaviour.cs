﻿using System.Collections.Generic;
using UnityEngine;

public class LocateAndSearchBehaviour : MonoBehaviour, IGuardBehaviour
{
    protected Dictionary<string, IGuardState> _locateAndSearchStatesDict = new Dictionary<string, IGuardState>();
    [SerializeField] protected LocateAndSearchSequenceState _locateAndSearchSequenceState;
    [SerializeField] protected FindPlayerPositionState _findPlayerPositionState;
    [SerializeField] protected WalkToTargetState _walkToTargetState;
    [SerializeField] protected LookAroundState _lookAroundState;

    [SerializeField] protected DisbeliefBehaviour _disbeliefBehaviour;


    ///// START /////
    void Start()
    {
        _locateAndSearchStatesDict.Add("LocateAndSearch", _locateAndSearchSequenceState);
        _locateAndSearchStatesDict.Add("FindPlayerPosition", _findPlayerPositionState);
        _locateAndSearchStatesDict.Add("WalkToTarget", _walkToTargetState);
        _locateAndSearchStatesDict.Add("LookAround", _lookAroundState);
    }


    public void EnterBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _locateAndSearchStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.EnterState();
                break;
            }
        }
    }


    public void UpdateBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _locateAndSearchStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.UpdateState();
                break;
            }
        }
    }


    public IGuardBehaviour GetNextBehaviour()
    {
        return _disbeliefBehaviour;
    }


    public string GetBehaviourTrigger()
    {
        return "LocateAndSearch";
    }
}