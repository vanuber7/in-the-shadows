﻿using System.Collections.Generic;
using UnityEngine;

public class PatrollingBehaviour : MonoBehaviour, IGuardBehaviour
{
    protected Dictionary<string, IGuardState> _guardPatrolStatesDict = new Dictionary<string, IGuardState>();
    [SerializeField] protected PatrolSequenceState _guardPatrolSequenceState;
    [SerializeField] protected FindNextTargetState _guardFindNextTargetState;
    [SerializeField] protected WalkToTargetState _guardWalkToTargetState;
    [SerializeField] protected WaitAtTargetState _guardWaitAtTargetState;


    ///// START /////
    void Start()
    {
        _guardPatrolStatesDict.Add("Patrol", _guardPatrolSequenceState);
        _guardPatrolStatesDict.Add("FindNextTarget", _guardFindNextTargetState);
        _guardPatrolStatesDict.Add("WalkToTarget", _guardWalkToTargetState);
        _guardPatrolStatesDict.Add("WaitAtTarget", _guardWaitAtTargetState);
    }


    public void EnterBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _guardPatrolStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.EnterState();
                break;
            }
        }
    }


    public void UpdateBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _guardPatrolStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.UpdateState();
                break;
            }
        }
    }


    public IGuardBehaviour GetNextBehaviour()
    {
        return null;
    }


    public string GetBehaviourTrigger()
    {
        return "Patrol";
    }
}