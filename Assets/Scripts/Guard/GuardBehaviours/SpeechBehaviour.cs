﻿using System.Collections.Generic;
using UnityEngine;

public class SpeechBehaviour : MonoBehaviour, IGuardBehaviour
{
    protected Dictionary<string, IGuardState> _speechStatesDict = new Dictionary<string, IGuardState>();
    [SerializeField] protected SpeechSequenceState _speechSequenceState;
    [SerializeField] protected SpeakState _speakState;
    [SerializeField] protected StopState _stopState;
    [SerializeField] protected WaitForSpeechState _waitState;

    // Needed for reference to behaviour after this one
    [SerializeField] protected LocateAndSearchBehaviour _locateAndSearchBehaviour;


    void Start()
    {
        _speechStatesDict.Add("Speech", _speechSequenceState);
        _speechStatesDict.Add("Speak", _speakState);
        //_speechStatesDict.Add("Stop", _stopState);
        //_speechStatesDict.Add("WaitForSpeech", _waitState);
    }


    public void EnterBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _speechStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.EnterState();
                break;
            }
        }
    }


    public void UpdateBehaviour(AnimatorStateInfo stateInfo)
    {
        foreach (KeyValuePair<string, IGuardState> pair in _speechStatesDict)
        {
            if (stateInfo.IsName(pair.Key))
            {
                pair.Value.UpdateState();
                break;
            }
        }
    }


    public IGuardBehaviour GetNextBehaviour()
    {
        return _locateAndSearchBehaviour;
    }


    public string GetBehaviourTrigger()
    {
        return "Speech";
    }
}
