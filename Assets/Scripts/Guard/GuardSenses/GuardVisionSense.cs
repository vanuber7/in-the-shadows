﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class GuardVisionSense : MonoBehaviour
{
	public event OnSawObject onSawObject;
	public delegate void OnSawObject(GameObject objectSeen);

	[SerializeField] protected Transform _guardTransform;
	[SerializeField] protected Transform _eye;

	[SerializeField] protected BoxCollider _sightBox;
	[SerializeField, Range(1.0f, 20.0f)] protected float _sightLength;
	[SerializeField, Range(0.05f, 1.0f)] protected float _sightCheckInterval;
    [SerializeField] protected Vector3 _shortSightRange;
    [SerializeField] protected Vector3 _normalSightRange;
    [SerializeField] protected Vector3 _largeSightRange;
    [SerializeField] protected Vector3 _hugeSightRange;

    [SerializeField] protected LayerMask _humanMask;
	[SerializeField] protected LayerMask _solidObjectsMask;

	protected bool _hasSeenHuman;


	///// START /////
	void Start()
    {
		_hasSeenHuman = false;

        StartCoroutine(CheckSightBox());
    }


	///// CHECK SIGHT BOX /////
	protected IEnumerator CheckSightBox()
    {
        while(true)
        {	
			// Ensure the box is directly ahead of us
			Vector3 boxCenter = _guardTransform.position + (_guardTransform.localRotation * _sightBox.center);
			// Look for humans
			foreach (Collider col in Physics.OverlapBox(boxCenter, _sightBox.size / 2.0f, _guardTransform.localRotation, _humanMask))
			{
				// Focus on objects that are not us!
				if (col.gameObject != this.transform.parent.gameObject)
				{
					CheckOtherBodyParts(col.gameObject);
				}
			}

            yield return new WaitForSeconds(_sightCheckInterval);
        }  
    }


	///// CHECK OTHER BODY PARTS /////
	protected void CheckOtherBodyParts(GameObject humanSensed)
    {
        foreach (BodyPart bodypart in humanSensed.GetComponentsInChildren<BodyPart>())
        {
			Transform bodyPartTransform = bodypart.GetComponent<Transform>();

			// Can we see this specific bodypart?
			Collider colliderHit = Raycaster.Raycast(this.gameObject, _eye, bodyPartTransform, _sightLength, _solidObjectsMask);

			if(colliderHit != null)
            {
				switch (colliderHit.tag)
				{
					case "Player":
						Raycaster.DrawRay(_eye, bodyPartTransform, _sightLength, Color.green, 0.5f);
						NotifyObjectSeen(colliderHit.gameObject);
                        bodypart.Display();
                        break;

					case "Guard":
						Raycaster.DrawRay(_eye, bodyPartTransform, _sightLength, Color.green, 0.5f);
						NotifyObjectSeen(colliderHit.gameObject);
						bodypart.Display();
						break;

                        // No human in sight
					case "Wall":
						Raycaster.DrawRay(_eye, bodyPartTransform, _sightLength, Color.red, 0.5f);
						break;
				}
            }

			if(_hasSeenHuman)
			{
				// We've seen a bodypart. No need to check the others
				_hasSeenHuman = false;
				break;
			}
        }
    }


	///// NOTIFY OBJECT SEEN /////
	protected void NotifyObjectSeen(GameObject objectSeen)
	{
		if(onSawObject != null)
		{
			_hasSeenHuman = true;
			// Inform the 'brain' about what we saw
			onSawObject(objectSeen);
		}
	}


    ///// CHANGE VISION RANGE /////
    public void ChangeVisionRange(float alertness)
    {
        // Increase/decrease the size of the vision box depending on guard's alertness
        if (alertness < 0.1f)
        {
            _sightBox.size = _shortSightRange;
            _sightBox.center = new Vector3(0.0f, 0.0f, _shortSightRange.z / 2.0f);
            _sightLength = _shortSightRange.z;
        }
        else if (alertness >= 0.1f && alertness < 0.4f)
        {
            _sightBox.size = _normalSightRange;
            _sightBox.center = new Vector3(0.0f, 0.0f, _normalSightRange.z / 2.0f);
            _sightLength = _normalSightRange.z;
        }
        else if (alertness >= 0.4f && alertness < 0.8f)
        {
            _sightBox.size = _largeSightRange;
            _sightBox.center = new Vector3(0.0f, 0.0f, _largeSightRange.z / 2.0f);
            _sightLength = _largeSightRange.z;
        }
        else if (alertness >= 0.8f)
        {
            _sightBox.size = _hugeSightRange;
            _sightBox.center = new Vector3(0.0f, 0.0f, _hugeSightRange.z / 2.0f);
            _sightLength = _hugeSightRange.z;
        }
    }
}