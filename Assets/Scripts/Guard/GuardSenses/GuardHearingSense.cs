﻿using UnityEngine;

public class GuardHearingSense : MonoBehaviour
{
    public event OnHeardSound onHeardSound;
    public delegate void OnHeardSound(AudioSource soundHeard);

    [SerializeField] protected SphereCollider _hearingTrigger;
    [SerializeField, Range(0.0f, 5.0f)] protected float _smallHearingRange;
    [SerializeField, Range(0.0f, 5.0f)] protected float _normalHearingRange;
    [SerializeField, Range(0.0f, 5.0f)] protected float _largeHearingRange;
    [SerializeField, Range(0.0f, 5.0f)] protected float _hugeHearingRange;


	/////  ON TRIGGER ENTER /////
	protected void OnTriggerEnter(Collider other)
	{
		// Check this is a valid sound
		AudioSource sound = other.GetComponent<AudioSource>();

		if(sound != null)
		{
			NotifyHeardSound(sound);
		}
	}


	///// NOTIFY SOUND HEARD /////
	protected void NotifyHeardSound(AudioSource soundHeard)
	{
		if (onHeardSound != null)
		{
			onHeardSound(soundHeard);
		}
	}


    public void ChangeHearingRadius(float alertness)
    {
        if(alertness < 0.1f)
        {
            _hearingTrigger.radius = _smallHearingRange;
        }
        else if(alertness >= 0.1f && alertness < 0.4f)
        {
            _hearingTrigger.radius = _normalHearingRange;
        }
        else if(alertness >= 0.4f && alertness < 0.8f)
        {
            _hearingTrigger.radius = _largeHearingRange;
        }
        else if(alertness >= 0.8f)
        {
            _hearingTrigger.radius = _hugeHearingRange;
        }
    }
}
