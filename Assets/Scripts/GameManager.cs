﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;

    public static bool _isGameOver;


	///// AWAKE /////
	void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }


	///// START /////
	void Start()
    {
		_isGameOver = false;

        // Subscribe to the predefined Unity new scene event
        SceneManager.sceneLoaded += OnNewSceneLoaded;
    }


    public static GameManager GetInstance()
    {
        if(_instance == null)
        {
            _instance = FindObjectOfType<GameManager>();
        }
        return _instance;
    }


	void OnNewSceneLoaded(Scene newScene, LoadSceneMode mode)
    {
        // Reset when a scene is loaded
		_isGameOver = false;
        Time.timeScale = 1.0f;
    }


	public void PlayGame()
    {
        // Main menu 'Play' and 'Replay'
        UIManager.GetInstance().TurnUIOff();
        SceneManager.LoadScene("Game");
    }


    ///// TRIGGER GAME ENDED /////
    public void TriggerGameEnded(bool won)
    {
        _isGameOver = true;
        Time.timeScale = 0.0f;

		if(won)
		{
            UIManager.GetInstance().ShowWinText();
        }
        else
		{
            UIManager.GetInstance().ShowLoseText();
        }
    }


	///// QUIT /////
	public void Quit()
	{
		Application.Quit();
	}


	///// ON DESTROY /////
	protected void OnDestroy()
    {
        // Unsubscribe from the scene change event
        SceneManager.sceneLoaded -= OnNewSceneLoaded;
    }
}