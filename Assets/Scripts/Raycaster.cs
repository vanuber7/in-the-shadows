﻿using UnityEngine;

public static class Raycaster
{
	///// RAYCAST /////
	public static Collider Raycast(GameObject instigator, Transform origin, Transform target, float rayLength, LayerMask layersToCheck)
	{
		Ray ray = new Ray(origin.position, target.position - origin.position);
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, rayLength, layersToCheck.value, QueryTriggerInteraction.Ignore))
		{
			// Ensure the ray hasn't hit ourselves
			if(hit.collider.gameObject != instigator)
			{
				return hit.collider;
			}
		}
		return null;
	}


	///// DRAW RAY /////
	public static void DrawRay(Transform origin, Transform target, float rayLength, Color rayColour, float duration = 0.0f)
	{
		Ray ray = new Ray(origin.position, target.position - origin.position);

		Debug.DrawRay(ray.origin, ray.direction * rayLength, rayColour, duration);
	}
}
