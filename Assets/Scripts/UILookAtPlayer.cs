﻿using UnityEngine;

public class UILookAtPlayer : MonoBehaviour
{
    [SerializeField] protected RectTransform _guardUITransform;
    [SerializeField] protected Transform _playerTransform;


    void Start()
    {
        _guardUITransform.rotation = Quaternion.identity;
    }


    void Update()
    {
        _guardUITransform.LookAt(_playerTransform);
    }
}
