﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance = null;

    [SerializeField] protected Text _titleText;
    [SerializeField] protected Image _playButton;
    [SerializeField] protected Text _playButtonText;
    [SerializeField] protected Image _replayButton;
    [SerializeField] protected Text _replayButtonText;
    [SerializeField] protected Image _quitButton;
    [SerializeField] protected Text _quitButtonText;
    [SerializeField] protected Image _controlsButton;
    [SerializeField] protected Text _controlsButtonText;
    [SerializeField] protected Image _instructionsButton;
    [SerializeField] protected Text _instructionsButtonText;
    [SerializeField] protected Text _controlsText;
    [SerializeField] protected Text _instructionsText;
    [SerializeField] protected Text _winText;
    [SerializeField] protected Text _loseText;


    ///// AWAKE /////
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }


    void Start()
    {
        _replayButton.enabled = false;
        _replayButtonText.enabled = false;
        _controlsText.enabled = false;
        _instructionsText.enabled = false;
        _winText.enabled = false;
        _loseText.enabled = false;
    }


    public static UIManager GetInstance()
    {
        if (_instance == null)
        {
            _instance = FindObjectOfType<UIManager>();
        }
        return _instance;
    }


    public void TurnUIOff()
    {
        _titleText.enabled = false;
        _playButton.enabled = false;
        _playButtonText.enabled = false;
        _replayButton.enabled = false;
        _replayButtonText.enabled = false;
        _quitButton.enabled = false;
        _quitButtonText.enabled = false;
        _controlsButton.enabled = false;
        _controlsButtonText.enabled = false;
        _instructionsButton.enabled = false;
        _instructionsButtonText.enabled = false;
        _controlsText.enabled = false;
        _instructionsText.enabled = false;
        _winText.enabled = false;
        _loseText.enabled = false;
    }


    public void ToggleControls()
    {
        _controlsText.enabled = !_controlsText.enabled;
    }


    public void ToggleInstructions()
    {
        _instructionsText.enabled = !_instructionsText.enabled;
    }


    public void ShowWinText()
    {
        _winText.enabled = true;
        ShowEndGameMenu();
    }


    public void ShowLoseText()
    {
        _loseText.enabled = true;
        ShowEndGameMenu();
    }


    protected void ShowEndGameMenu()
    {
        _replayButton.enabled = true;
        _replayButtonText.enabled = true;
        _quitButton.enabled = true;
        _quitButtonText.enabled = true;
    }
}
