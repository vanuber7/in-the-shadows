﻿using UnityEngine;

public class SoundDestroyer : MonoBehaviour
{
	[SerializeField] protected AudioSource _sound;

	void Start()
	{
		Destroy(gameObject, _sound.clip.length);
	}
}