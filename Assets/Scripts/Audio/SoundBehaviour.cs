﻿using UnityEngine;

public class SoundBehaviour : MonoBehaviour
{
	public float _lowestPitch;
	public float _highestPitch;
	[SerializeField] protected AudioSource _sound;

	///// START /////
	void Start()
	{
		ChangePitch();
		_sound.Play();
	}


	///// CHANGE PITCH /////
	protected void ChangePitch()
	{
		_sound.pitch = Random.Range(_lowestPitch, _highestPitch);
	}
}