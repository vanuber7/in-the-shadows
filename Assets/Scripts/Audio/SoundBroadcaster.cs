﻿using UnityEngine;

public class SoundBroadcaster : MonoBehaviour
{
	[SerializeField] protected AudioSource _sound;
	[SerializeField] protected SphereCollider _trigger;

	void Start()
	{
		// Ensure the broadcast radius matches the sounds full range
		_trigger.radius = _sound.maxDistance;
	}
}
