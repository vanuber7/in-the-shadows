﻿using System.Collections.Generic;
using UnityEngine;
using Guard;


public class AudioManager : MonoBehaviour
{
    [SerializeField] protected List<AudioSource> _soundPrefabs;

    private static AudioManager _instance = null;


    ///// AWAKE /////
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }


    ///// GET INSTANCE /////
    public static AudioManager GetInstance()
    {
        // Only one of us should exist!
        if (_instance == null)
        {
            _instance = FindObjectOfType<AudioManager>();
        }
        return _instance;
    }


    ///// PLAY SOUND /////
    public BehaviourTreeStatus PlaySound3D(string clipToPlay, Vector3 position)
    {
        // Find this clip
        foreach (AudioSource sound in _soundPrefabs)
        {
            if (sound.name == clipToPlay)
            {
                Instantiate(sound, position, Quaternion.identity);
                return BehaviourTreeStatus.SUCCESS;
            }
        }
        return BehaviourTreeStatus.FAILURE;
    }


    public float GetClipLength(string clipToPlay)
    {
        foreach (AudioSource sound in _soundPrefabs)
        {
            if (sound.name == clipToPlay)
            {
                return sound.clip.length;
            }
        }
        return 0.0f;
    }


    ///// PAUSE AUDIO ///// // Need a reference to this!!
    public void PauseAudio()
    {
        foreach (AudioSource sound in _soundPrefabs)
        {
            if (sound.isPlaying)
            {
                sound.Pause();
            }
        }
    }


    ///// RESUME AUDIO ///// // Need a reference to this!!
    public void ResumeAudio()
    {
        foreach (AudioSource sound in _soundPrefabs)
        {
            if (!sound.isPlaying)
            {
                sound.UnPause();
            }
        }
    }
}