﻿using UnityEngine;

public class PlayerDistractions : MonoBehaviour, IInteractable
{
    [SerializeField] protected Quotes _distractions;


    public void Interact()
	{
		Debug.Log("Interacted with " + this.gameObject);
	}


    private void Update()
    {
        if (Input.GetButtonUp("Whistle") || Input.GetMouseButtonDown(0))
        {
            if (!GameManager._isGameOver)
            {
                int numDistractions = _distractions._quotes.Count;

                // Choose a random distraction sound to play
                if (numDistractions > 0)
                {
                    int soundToPlay = Random.Range(0, _distractions._quotes.Count);
                    AudioManager.GetInstance().PlaySound3D(_distractions._quotes[soundToPlay].name, transform.position);
                }
            }
        }
    }
}
