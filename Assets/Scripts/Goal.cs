﻿using UnityEngine;

public class Goal : MonoBehaviour
{
    protected void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Won the game
            Debug.Log("You Win!!");
            GameManager.GetInstance().TriggerGameEnded(true);
        }
    }
}
