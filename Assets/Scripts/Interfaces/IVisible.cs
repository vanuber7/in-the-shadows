﻿public interface IVisible
{
	void Show();
}