﻿using UnityEngine;

public interface IGuardBehaviour
{
    void EnterBehaviour(AnimatorStateInfo stateInfo);

    void UpdateBehaviour(AnimatorStateInfo stateInfo);

    IGuardBehaviour GetNextBehaviour();

    string GetBehaviourTrigger();
}
