﻿public interface IGuardState
{
    void EnterState();
    void UpdateState();
}
