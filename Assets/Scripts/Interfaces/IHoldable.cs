﻿public interface IHoldable
{
	void PickUp();
	void Throw();
	void Use();
}
